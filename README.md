# Azure Functions: Intro to Expert(ish)

## About

This repo contains the source and scripts for generating class, lecture, and presentation docs.

The purpose is to use Markdown text files to create lecture notes, and then use pandoc to create HTML, PDF, and Powerpoint slides from those notes.

The source and scripts require pandoc and latex for generating output.

https://pandoc.org/

https://miktex.org/

You can use chocolatey to install both easily.

https://chocolatey.org/packages?q=latex

## Hierarchy

`bin`
Scripts for creating files.

`builds`
Files that have been generated.

`src`
Source files.

## Release Notes:

v0.1.0: Initial creation.

v1.0.0: FWDNUG Presentation
