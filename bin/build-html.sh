echo "Building Talk HTML..."

pandoc ../src/AzureFunctions-IntroToExpertish-Talk.md -f markdown -t html --metadata pagetitle="Azure Functions: Intro to Expert(ish) in 60 Minutes" -s -o ../builds/AzureFunctions-IntroToExpertish-Talk.html