echo "Building all files..."

./build-abstract-pdf.sh
./build-abstract-pptx.sh

./build-beamer.sh
./build-html.sh
./build-pdf.sh
./build-pptx.sh
./build-s5.sh

echo "Done with building all files."