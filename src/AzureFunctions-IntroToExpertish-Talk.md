<!--
% Azure Functions: Intro to Expert(ish) in 60 Minutes
% Mark Leon Watson
% February 19, 2019
% Version 1.0
-->

--------------------------------------------------------------------------------

# Azure Functions: Intro to Expert(ish) in 60 Minutes

![](../src/images/azurefunctions-logo.jpeg)

<!--
An Hour of Lightning Talks ;)

FWDNUG February 19, 2019

Agenda

00:00 - Talk: Intro  
00:05 - Talk: Overview  
00:10 - Level 100: Introduction to Serverless and Event-Based Processing and Consumption Models
00:15 - Level 100: Introduction to Azure Functions
00:20 - Level 100: Demos (Getting Started)
00:25 - Level 200: Intermediate  
00:30 - Level 200: Demos (Job Request Demo)
00:35 - Level 300: Advanced  
00:40 - Level 300: Demos (Event Logging Demo)
00:45 - Level Expertish: Summary 
00:50 - Level Expertish: Q&A
-->

--------------------------------------------------------------------------------

<!-- TALK: INTRO/ABSTRACT -->

--------------------------------------------------------------------------------

# Azure Functions: Intro to Expert(ish) in 60 Minutes

**Let's talk about Azure Functions!**  

This talk will start out with the basics of Azure Functions and then ramp up quickly on the when, where, why, and how to use them. 

It will also discuss platforms, toolsets, design and development, and tips learned from real world experiences. 

# Azure Functions: Intro to Expert(ish) in 60 Minutes

### Audience

This talk will be beneficial to persons both new to and already familiar with Azure Functions. 

Hopefully after this session you will feel like an expert, or at least expert-ish, on Azure Functions. 

# Azure Functions: Intro to Expert(ish) in 60 Minutes

### Presenter

Mark Leon Watson is a software engineering consultant with over 3 decades of professional experience in software development across many business domains and technologies. He is currently working as a Software Architect for Afterman Software and providing software architecture and development services to an insurance company based in Fort Worth, Texas.

<!--
### About the presenter...

- husband/father
- guitar player/"musician"
- fan of the outdoors
- continuous learner and improver
- seeker of truth, LOL!

- Glad to be back to speaking
- First time in a while
- First time giving this talk
- Be gentle

### Enough about the speaker...
-->

# Brought to you by Mountain Cedar and OTC Allergy Meds

![](../src/images/allergies.jpg)

# Housekeeping

"If you are driving, don't drink.

And if you are drinking, don't drive."

- Beastie Boys

--------------------------------------------------------------------------------

<!-- TALK: OVERVIEW OF PRESENTATION LEVELS -->

--------------------------------------------------------------------------------

# Standard Presentation Material Levels

- Level 100: Introductory and overview material. 

<!-- Assumes little or no expertise with topic and covers topic concepts, functions, features, and benefits. -->

- Level 200: Intermediate material. 

<!-- Assumes 100-level knowledge and provides specific details about the topic. -->

- Level 300: Advanced material. 

<!-- Assumes 200-level knowledge, in-depth understanding of features in a real-world environment, and strong coding skills. Provides a detailed technical overview of a subset of product/technology features, covering architecture, performance, migration, deployment, and development. -->

- Level 400: Expert material. 

<!-- Assumes a deep level of technical knowledge and experience and a detailed, thorough understanding of topic. Provides expert-to-expert interaction and coverage of specialized topics. -->

<!-- https://blogs.technet.microsoft.com/ieitpro/2006/09/29/microsofts-standard-level-definitions-100-to-400/ -->

# "expert"

noun
1. a person who has a comprehensive and authoritative knowledge of or skill in a particular area. *"a financial expert"*

<!--
synonyms: specialist, authority, pundit, oracle, resource person, ...

adjective
1. having or involving authoritative knowledge. "he had received expert academic advice" >> synonyms: skillful, skilled, adept, accomplished, talented, fine, ...

https://www.google.com/search?q=Dictionary#dobs=expert
-->

# "expertish"

Someone who has some expertise, but not a full blown expert.  

*"My coach is expertish, but he never played ball so he isn’t an expert."*

<!--
by mrmurpal02 July 05, 2018
https://www.urbandictionary.com/define.php?term=expertish
-->

<!--
# Gladwell's Definition

10,000 hours

### Definition of 'Expert' in flux?

New Study Destroys Malcolm Gladwell's 10,000 Hour Rule

https://www.businessinsider.com/new-study-destroys-malcolm-gladwells-10000-rule-2014-7

-->

# #Truthiness

noun: the quality of seeming or being felt to be true, even if not necessarily true.  

Origin early 19th century (in the sense ‘truthfulness’): coined in the modern sense by the US humorist Stephen Colbert.

<!-- https://www.google.com/search?q=Dictionary#dobs=truthiness -->

<!--
# Expertish Presentation Material Levels

A little bit of this and a little bit of that from levels 100-300.
-->

# 100% Satisfaction Guarantee


If you believe that by the end of this talk that you are not to the level of expertish...


...then I will refund 100% of the cost of your admission. ;)

--------------------------------------------------------------------------------

# Audience Gauge

![](../src/images/gauge-type1-20-500px.png)

<!--
- Why are we all here?

- What Azure Functions experience do you have?

- What are you currently using Azure Functions for? 
-->

--------------------------------------------------------------------------------

# Azure Functions: Level 100: Introductory

![](../src/images/azurefunctions-logo.jpeg)

# Serverless?

![](../src/images/empty-server-rack.jpg)

# Serverless

<center>*"provisioning, scaling, and management of resources is handled for you"*</center>

<!-- https://azure.microsoft.com/en-us/solutions/serverless/ -->

# Serverless in Azure

- fully managed services
- end-to-end development experience
- built on an open source foundation
- run serverless solutions anywhere

# Example Serverless Applications

- web app backend
- mobile app backend
- real-time file processing
- real-time stream processing

# Serverless Execution Environments

- Serverless functions (Azure Functions)
- Serverless containers (Azure Container Instances)
- Serverless Kubernetes orchestration (AKS)
- Serverless application environments (App Service)

# Fully Managed Services

- Serverless database (CosmosDB)
- Serverless messaging (Event Grid, Service Bus)
- Serverless workflow orchestration (Logic Apps)
- Serverless analytics (Azure Stream Analytics)
- Serverless intelligence (Cognitive Services)

# Service Fabric Mesh

*Azure Service Fabric Mesh is a fully managed service that enables developers to deploy microservices applications without managing virtual machines, storage, or networking.*

<!-- https://docs.microsoft.com/en-us/azure/service-fabric-mesh/service-fabric-mesh-overview -->

# Serverless Development Experience

## Developer Tools

- [Visual Studio developer tools for Functions](https://docs.microsoft.com/en-us/azure/azure-functions/functions-develop-vs)
- [Azure Functions extension for Visual Studio Code](https://code.visualstudio.com/tutorials/functions-extension/getting-started)
- [Visual Studio developer tools for Logic Apps](https://docs.microsoft.com/en-us/azure/logic-apps/quickstart-create-logic-apps-with-visual-studio)

## Continuous integration and continuous delivery

- [Azure DevOps](https://azure.microsoft.com/en-us/services/monitor/)

## Monitoring, logging, and diagnostics

- [Application Insights](https://azure.microsoft.com/en-us/services/monitor/)

<!--
# Azure Serverless Platform

- Service Fabric
- App Service
- Web App
-->

--------------------------------------------------------------------------------

<!--
# Azure Functions: Level 100: Introductory

# Azure Functions: Overview (MS)

https://docs.microsoft.com/en-us/azure/azure-functions/functions-overview
-->

# What is Azure Functions?

![](../src/images/azurefunctions-logo.jpeg)

<!--
# What is Azure Functions?

*Azure Functions is a solution for easily running **small pieces of code**, or "functions," in the cloud. You can write just the code you need for the problem at hand, without worrying about a whole application or the infrastructure to run it. Functions can make development even more productive, and you can use your development **language of choice**, such as C#, F#, Node.js, Java, or PHP. **Pay only for the time your code runs** and trust Azure to **scale as needed**. Azure Functions lets you develop **serverless** applications on Microsoft Azure.*
-->

# What is Azure Functions?

- small pieces of code
- language of choice
- pay only for the time your code runs
- scale as needed
- serverless

# Azure Functions: Features

- Choice of language
- Pay-per-use pricing model
- Bring your own dependencies
- Integrated security
- Simplified integration
- Flexible development
- Open-source

# Azure Functions: Supported Languages

- C#
- Javascript
- F#
- Java*
- Python*
- TypeScript*
- PHP*
- Batch*
- Bash*
- Powershell*

* Preview, experimental, or V1 supported-only.

<!-- https://docs.microsoft.com/en-us/azure/azure-functions/supported-languages -->

# Azure Functions: Integrations

- Azure Cosmos DB
- Azure Event Hubs
- Azure Event Grid
- Azure Notification Hubs
- Azure Service Bus (queues and topics)
- Azure Storage (blob, queues, and tables)
- On-premises (using Service Bus)
- Twilio (SMS messages)

# "EVENT DRIVEN"

- HTTPTrigger
- TimerTrigger
- CosmosDBTrigger
- BlobTrigger
- QueueTrigger
- EventGridTrigger
- EventHubTrigger
- ServiceBusQueueTrigger
- ServiceBusTopicTrigger

# Azure Functions: Hosting Plans

- Consumption plan
- App Service plan

# Consumption plan

- hosts are dynamically added and removed based on the number of incoming events
- serverless plan scales automatically
- charged for compute resources only when functions are running
- execution times out after a configurable period of time

<!--
*When you're using a Consumption plan, instances of the Azure Functions host are dynamically added and removed based on the number of incoming events. This serverless plan scales automatically, and you're charged for compute resources only when your functions are running. On a Consumption plan, a function execution times out after a configurable period of time.*
-->

# App Service Plan

- function apps run on dedicated VMs
- same as other App Service apps
- Dedicated VMs are allocated to your function app
- host can be always running
- supports Linux

<!--
*In the dedicated App Service plan, your function apps run on dedicated VMs on Basic, Standard, Premium, and Isolated SKUs, which is the same as other App Service apps. Dedicated VMs are allocated to your function app, which means the functions host can be always running. App Service plans support Linux.*
-->

<!--
# App Service

https://azure.microsoft.com/en-us/services/app-service/?v=18.51
-->

# What can I do with Azure Functions?

- processing data
- integrating systems
- working with the internet-of-things (IoT)
- building simple APIs and microservices

<!--
Resources:
- https://docs.microsoft.com/en-us/azure/azure-functions/functions-scale
- https://azure.microsoft.com/en-us/pricing/details/functions/
-->

<!--
## Overview (MLW)

#### Where?

Where can I use Azure Functions?

- Azure 
- Linux
- Local Visual Studio

#### Why? 

Why should I use Azure Functions?

- PaaS
- Automatic Scaling
- Consumption Based Scaling and Pricing
- Microservices
- Batch Processing
- Message Pipelines

#### When?

When should I use Azure Functions?

When should I use consumption vs app plan?

-->

# Azure Functions: Level 100

## Summary

- Concepts
- Features
- Benefits

# Congratulations! Level 100 Achieved!

![](../src/images/level-up.jpg)


--------------------------------------------------------------------------------

# Azure Functions: Level 200: Intermediate

![](../src/images/azurefunctions-logo.jpeg)

# Overview

Level 200: Intermediate material.  

Assumes 100-level knowledge and provides specific details about the topic.

# Overview

- Development
- Tools
- Templates

# How do I create Azure Functions?

- Azure Portal Development
- Local Development 

# How do I create Azure Functions?

For developing locally and deploying remotely, you'll need to:  

- create your function code
- test and debug your functions
- deploy your tested functions*

*You'll need an Azure account and subscription to deploy to Azure.

# Development Tools

## IDEs
- [Visual Studio](https://visualstudio.microsoft.com/)
- [Visual Studio Code](https://code.visualstudio.com/)

## Extension Tools
- [Visual Studio developer tools for Functions](https://docs.microsoft.com/en-us/azure/azure-functions/functions-develop-vs)
- [Azure Functions extension for Visual Studio Code](https://code.visualstudio.com/tutorials/functions-extension/getting-started)

# Development Tools

## CLI Tools
- [.NET Core Tools CLI](https://docs.microsoft.com/en-us/dotnet/core/tools/)
- [Azure Powershell](https://docs.microsoft.com/en-us/powershell/azure/)
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/)

# Project Templates

- HTTPTrigger
- TimerTrigger
- CosmosDBTrigger
- BlobTrigger
- QueueTrigger
- EventGridTrigger
- EventHubTrigger
- ServiceBusQueueTrigger
- ServiceBusTopicTrigger

# Run()

It all starts with Run()! (or RunAsync())

<!-- https://docs.microsoft.com/en-us/azure/azure-functions/functions-create-first-azure-function -->

# DEMO: HttpTrigger

The HTTP trigger lets you invoke a function with an HTTP request. 

You can use an HTTP trigger to build serverless APIs and respond to webhooks.

<!-- https://docs.microsoft.com/en-us/azure/azure-functions/functions-bindings-http-webhook -->

# DEMO: HttpTrigger

![](../src/images/demo.png)

# DEMO: TimerTrigger

Azure Functions uses the NCronTab library to interpret CRON expressions. A CRON expression includes six fields:

{second} {minute} {hour} {day} {month} {day-of-week}

<!-- https://docs.microsoft.com/en-us/azure/azure-functions/functions-bindings-timer -->

# DEMO: TimerTrigger

![](../src/images/demo.png)

# Application Settings

- local vs deployed
- settings.json file vs. environment vars
- Key Vaults

<!--
# .net core vs net. standard

# Is it an App or Library?
-->

# DEMO: Ship It!

![](../src/images/demo.png)

<!--

### Design Considerations

#### Microservices

- Shared Code

#### Limitations

#### Hybrid Connections

#### V1 vs V2

#### Nuget Packages

#### ILogger vs ITraceWriter

#### Configuration Building

#### Developer Resources

https://docs.microsoft.com/en-us/azure/azure-functions/functions-reference

#### Testing Resources

https://docs.microsoft.com/en-us/azure/azure-functions/functions-test-a-function

#### Learning Resources

-->

# Azure Functions: Level 200: Intermediate

## Summary

- Details
- Development Tools
- Creating Azure Functions

# Congratulations! Level 200 Achieved!

![](../src/images/level-up.jpg)

--------------------------------------------------------------------------------

# Azure Functions: Level 300: Advanced!

![](../src/images/azurefunctions-logo.jpeg)

# Overview

Level 300: Advanced material. 

Assumes 200-level knowledge, in-depth understanding of features in a real-world environment, and strong coding skills. Provides a detailed technical overview of a subset of product/technology features, covering architecture, performance, migration, deployment, and development.

### Let's build some things!

# Durable Functions

*Durable Functions are an extension of Azure Functions that lets you write stateful functions in a serverless environment. The extension manages state, checkpoints, and restarts for you.*

<!-- https://docs.microsoft.com/en-us/azure/azure-functions/durable/durable-functions-overview -->

# Application Patterns

The primary use case for Durable Functions is simplifying complex, stateful coordination requirements in serverless applications. The following are some typical application patterns that can benefit from Durable Functions:

- Chaining
- Fan-out/fan-in
- Async HTTP APIs
- Monitoring
- Human interaction

<!--
https://docs.microsoft.com/en-us/azure/azure-functions/durable/durable-functions-overview
https://docs.microsoft.com/en-us/azure/azure-functions/durable/durable-functions-concepts#chaining
https://docs.microsoft.com/en-us/dotnet/standard/serverless-architecture/
-->

# Example Chaining Pattern

![](../src/images/function-chaining.png)

<!-- https://azure.microsoft.com/en-us/blog/simplifying-security-for-serverless-and-web-apps-with-azure-functions-and-app-service/ -->

# Access Security

- anonymous
- function
- admin

# Authentication

- [Azure AD](https://docs.microsoft.com/en-us/azure/app-service/configure-authentication-provider-aad)
- [Facebook](https://docs.microsoft.com/en-us/azure/app-service/configure-authentication-provider-facebook)
- [Google](https://docs.microsoft.com/en-us/azure/app-service/configure-authentication-provider-google)
- [Twitter](https://docs.microsoft.com/en-us/azure/app-service/configure-authentication-provider-twitter)
- [Managed Identities](https://docs.microsoft.com/en-us/azure/app-service/overview-managed-identity)
- [API Management](https://stackoverflow.com/questions/46617942/what-are-the-ways-to-secure-azure-functions)
- [White Listing](https://www.linkedin.com/pulse/whitelisting-azure-functions-mike-douglas)

# DEMO: Job Request System

![](../src/images/JobRequestsDemo-draw.io.jpg)

# DEMO: Job Request System

![](../src/images/demo.png)

# DEMO

![](../src/images/MLW-Demos-Azure-Functions-EventLogging-Diagram-draw.io.jpg)

# DEMO: Event Logging and Notification System

![](../src/images/demo.png)

# TIP: Use log4net *AND* Microsoft Logging

Nuget Package: 

Microsoft.Extensions.Logging.Log4Net.AspNetCore

# TIP: .net Standard, NOT .net Core

Some Azure Functions templates have the TargetFramework set to netcoreapp2.1.

Before deploying to Azure, you'll need to set this to netstandard2.0.

# TIP: Change to V2 when publishing to Azure

You'll need to specify V2 when publishing you functions to Azure.

# TIP: When do I HAVE to use an App Service Hosting Plan?

## Hybrid Database Connections

- onsite databases require a hybrid connection, which will required an app service plan
- older server OS will not work
	
# Azure Functions: Level 300: Advanced

## Summary

- Durable Functions
- Patterns
- Security
- Chaining Demos
- Tips

# Congratulations! Level 300 Achieved!

![](../src/images/level-up.jpg)

--------------------------------------------------------------------------------

# Azure Functions: Intro to Expert(ish)

|**Level 100:**|**Level 200:** |**Level 300:**   |
|--------------|---------------|-----------------|
|- Intro       |- Intermediate |- Advanced       |
|- Concepts    |- Details      |- Patterns       |
|- Features    |- Creation     |- Security       |
|- Benefits    |- Deployment   |- Tips           |

--------------------------------------------------------------------------------

# Congratulations!<br/>Expertish Level Achieved!

![](../src/images/level-up.jpg)

--------------------------------------------------------------------------------

# Azure Functions: Learning Resources

- [Microsoft Docs](https://docs.microsoft.com/en-us/azure/azure-functions/)

- [Udemy](https://www.udemy.com/)
	- Scott Duffy Courses on Azure

- Packt Publishing
	- [Azure Serverless Computing Cookbook](https://azure.microsoft.com/en-us/resources/azure-serverless-computing-cookbook/)

- Microsoft Publishing	
	- [Serverless Apps: Architecture, Patterns, and Azure Implementation](https://aka.ms/serverless-ebook)
	
- [Safari Books Online ($)](https://safaribooksonline.com)

--------------------------------------------------------------------------------

# Azure Functions: Intro to Expert(ish)

![info@markwatson.net](../src/images/thank-you.jpg)

--------------------------------------------------------------------------------

# Azure Functions: Intro to Expert(ish)

**Presentation Files**
https://bitbucket.org/MarkLeon/mlw-talk-azurefunctions-intro2expertish

**Job Requests Demo**
https://bitbucket.org/MarkLeon/mlw-demos-azurefunctions-jobrequests

**Event Logging Demos**
https://bitbucket.org/MarkLeon/mlw-demos-azurefunctions-eventlogging

--------------------------------------------------------------------------------

# 

![](../src/images/watchmen-smiley.jpg)

