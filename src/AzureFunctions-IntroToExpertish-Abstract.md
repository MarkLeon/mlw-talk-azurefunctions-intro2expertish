<!--
% Azure Functions: Intro to Expert(ish) in 60 Minutes - Abstract
% Mark Leon Watson
% February 19, 2019
% Version 1.0
-->

# Azure Functions: Intro to Expert(ish) in 60 Minutes

![](../src/images/azurefunctions-logo.jpeg)

# Let's talk about Azure Functions! 

This talk will start out with the basics of Azure Functions and then ramp up quickly on the when, where, why, and how to use them. 

It will also discuss platforms, toolsets, design and development, and tips learned from real world experiences. 

# Audience

![](../src/images/who-me-minion.jpg)

# Yes, YOU! 

This talk will be beneficial to persons both new to and already familiar with Azure Functions. 

Hopefully after this session you will feel like an expert, or at least expert-ish, on Azure Functions. 

# Speaker

Mark Leon Watson is a software engineering consultant with over 3 decades of professional experience in software development across many business domains and technologies. He is currently working as a Software Architect for Afterman Software and providing software architecture and development services to an insurance company based in Fort Worth, Texas.

# Brought to you by... Mountain Cedar

![](../src/images/allergies.jpg)

# Shout out!

![](../src/images/fwdnug-logo.png)

# Shout out!

![](../src/images/teksystems-logo.png)

# Shout out!

![](../src/images/aftermansoftware-logo.png)

# Housekeeping

"If you are driving, don't drink.

And if you are drinking, don't drive."

- Beastie Boys

![](../src/images/beastie-boys.jpg)
